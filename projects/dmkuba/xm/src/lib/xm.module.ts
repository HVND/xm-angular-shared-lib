import { NgModule } from '@angular/core';
import { XmComponent } from './xm.component';



@NgModule({
  declarations: [
    XmComponent
  ],
  imports: [
  ],
  exports: [
    XmComponent
  ]
})
export class XmModule { }
