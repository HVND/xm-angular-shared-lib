import { TestBed } from '@angular/core/testing';

import { XmService } from './xm.service';

describe('XmService', () => {
  let service: XmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
