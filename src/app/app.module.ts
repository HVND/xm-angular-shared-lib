import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {XmModule} from "@dmkuba/xm";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        XmModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
